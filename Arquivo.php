<?php

namespace Rubeus\Arquivos;

use Rubeus\CloudStorage\CloudStorage;

class Arquivo
{
    public static function criaArquivo($conteudo, $caminho, $retornaConteudo = false, $tipoCaminho = 'limpo', $ambienteCrm = true)
    {
        if (is_resource($conteudo)) {
            $conteudo = stream_get_contents($conteudo);
        }

        if (CloudStorage::verificaPermissao()) {
            $tipoMime = self::recuperaTipoMime($caminho, $conteudo);
            if ($ambienteCrm) {
                $caminho = self::geraCaminho($caminho, 'limpo');
            }else{
                $caminho = self::geraCaminho($caminho, 'limpoSemFile');
            }
            CloudStorage::insereObjetoStorage($conteudo, $caminho, $tipoMime);
        } else {
            $caminho = self::geraCaminho($caminho, 'crmCaminho');
            if (!file_exists(dirname($caminho))) {
                mkdir(dirname($caminho));
            }
            file_put_contents($caminho, $conteudo);
        }

        return $retornaConteudo ? $conteudo : self::geraCaminho($caminho, $tipoCaminho);
    }

    public static function recuperaArquivo($caminho, $somenteConteudo = true, $tipoLeitura = 'r')
    {
        if (CloudStorage::verificaPermissao()) {
            $caminho = self::geraCaminho($caminho, 'limpo');
            if ($somenteConteudo) {
                $conteudo = CloudStorage::retornaObjeto($caminho);
            } else {
                $conteudo = fopen(
                    self::geraCaminho($caminho),
                    $tipoLeitura
                );
            }
        } else {
            $caminho = self::geraCaminho($caminho, 'crmCaminho');
            if (file_exists($caminho)) {
                $conteudo = $somenteConteudo ? file_get_contents($caminho) : fopen($caminho, $tipoLeitura);
            } else {
                $conteudo = null;
            }
        }
        return $conteudo;
    }

    public static function recuperaArquivoApp($caminho, $tipoCaminho, $somenteConteudo = true, $tipoLeitura = 'r')
    {
        if (CloudStorage::verificaPermissao()) {
            $caminho = self::geraCaminho($caminho, $tipoCaminho);
            if ($somenteConteudo) {
                $conteudo = CloudStorage::retornaObjeto($caminho);
            } else {
                $conteudo = fopen(
                    self::geraCaminho($caminho, $tipoCaminho),
                    $tipoLeitura
                );
            }
        } else {
            $conteudo = null;
        }
        return $conteudo;
    }

    public static function moveArquivo($caminhoOriginal, $caminhoNovo, $arquivoTemporario=true)
    {
        $caminhoNovo = self::geraCaminho($caminhoNovo, 'limpo');
        if (!$arquivoTemporario) {
            $caminhoOriginal = self::geraCaminho($caminhoOriginal, 'limpo');
        }

        try {
            $arquivoOriginal = file_get_contents($caminhoOriginal);
            $resultado = self::criaArquivo($arquivoOriginal, $caminhoNovo) ? true : false;
        } catch (\Exception $e) {
            $resultado = false;
        }

        return $resultado;
    }

    public static function recuperaTipoMime($caminho, $conteudo = false)
    {
        $tipoMime = false;
        if (is_resource($conteudo)) {
            $tipoMime = mime_content_type($conteudo);
        } elseif (file_exists($caminho)) {
            $streamArquivo = fopen($caminho, 'r');
            $tipoMime = mime_content_type($streamArquivo);
        }

        if (!$tipoMime) {
            $caminhoExplodido = explode('.', $caminho);
            $tipoArquivo = end($caminhoExplodido);
            $tipoMime = self::geraTipoMime($tipoArquivo);
        }
        return $tipoMime;
    }

    public static function geraTipoMime($tipoArquivo)
    {
        switch ($tipoArquivo) {
            case 'json':
                return 'application/json';
            case 'csv':
                return 'text/csv';
            case 'html':
                return 'text/html';
            case 'png':
                return 'image/png';
            case 'jpg':
                return 'image/jpeg';
            case 'gif':
                return 'image/gif';
            case 'wav':
                return 'audio/wav';
            case 'pdf':
                return 'application/pdf';
        }
    }

    public static function geraCaminho($caminho, $tipo = 'storage')
    {
        $caminhoExplodido = explode('file/', $caminho);
        $caminho = 'file/' . end($caminhoExplodido);
        $caminho = preg_replace('/\/{2,}/', '/', $caminho);
        switch ($tipo) {
            case 'crm':
                $dominioCrm = DOMINIO_PROJETO;
                return "$dominioCrm$caminho";
                break;
            case 'crmCaminho':
                $caminhoCrm = DIR_BASE;
                return "$caminhoCrm/$caminho";
                break;
            case 'storage':
                $dominioStorage =  DOMINIO_STORAGE;
                return $dominioStorage . $caminho;
                break;
            case 'storageSemFile':
                $dominioStorage =  DOMINIO_STORAGE;
                $caminhoSemFile = explode('file/', $caminho);
                $caminho = end($caminhoSemFile);
                return $dominioStorage . $caminho;
                break;
            case 'limpoSemFile':
                $caminhoSemFile = explode('file/', $caminho);
                $caminho = end($caminhoSemFile);
                return CloudStorage::verificaPermissao() ? $caminho : "/$caminho";
            case 'limpo':
                return CloudStorage::verificaPermissao() ? $caminho : "/$caminho";
            case 'url':
                return self::geraUrlBase() . $caminho;
        }
    }

    public static function geraUrlBase()
    {
        return CloudStorage::verificaPermissao() ? DOMINIO_STORAGE : DOMINIO_PROJETO;
    }

    public static function geraCaminhoCondicional($caminho)
    {
        if (CloudStorage::verificaPermissao()) {
            return self::geraCaminho($caminho);
        }
        return self::geraCaminho($caminho, 'crm');
    }

    public static function listaArquivos($caminho, $listarSubpastas = false)
    {
        if (CloudStorage::verificaPermissao()) {
            $caminhoFormatado = self::geraCaminho($caminho, 'limpo');
            $dir = CloudStorage::listaObjetosStorage(
                $caminhoFormatado
            );
            if ($listarSubpastas) {
                return $dir;
            }
            foreach ($dir as $arquivo) {
                if (CloudStorage::verificaArquivo($arquivo, $caminhoFormatado)) {
                    $arquivos[] = $arquivo;
                }
            }
        } else {
            $caminhoFormatado = self::geraCaminho($caminho, 'crmCaminho');
            $dir = scandir(
                $caminhoFormatado
            );
            if ($listarSubpastas) {
                return $dir;
            }
            foreach ($dir as $arquivo) {
                if (is_file($caminhoFormatado . '/' . $arquivo)) {
                    $arquivos[] = $arquivo;
                }
            }
        }
        if (isset($arquivos)) {
            return $arquivos;
        }
        return false;
    }

    public static function geraArquivoGaleria($arquivo, $caminhoUploads, $caminhoThumbnail)
    {
        if (CloudStorage::verificaPermissao()) {
            $name = explode('/', $arquivo->name)[3];
            $size = $arquivo->size;
        } else {
            $name = $arquivo;
            $file_path = self::geraCaminho($caminhoUploads.$name, 'crmCaminho');
            $size = filesize($file_path);
        }
        $file = [
            "name" => $name,
            "url" => self::geraCaminho($caminhoUploads, 'url').$name,
            "size" => $size
        ];
        $file["thumbnailUrl"] = self::geraCaminho($caminhoThumbnail, 'url').$name;
        return $file;
    }

    public static function geraCaminhoApp($caminho, $tipo = 'storage')
    {
        $caminho = preg_replace('/\/{2,}/', '/', $caminho);
        switch ($tipo) {
            case 'storage':
                $dominioStorage =  DOMINIO_STORAGE;
                return $dominioStorage . $caminho;
                break;
            case 'limpo':
                return CloudStorage::verificaPermissao() ? $caminho : "/$caminho";
            case 'url':
                return self::geraUrlBase() . $caminho;
        }
    }
}
